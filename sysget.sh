#!/bin/bash

# Check if the required arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: sysget [--cpu | --memory | --storage] <IP>"
    exit 1
fi

# Function to get CPU utilization
get_cpu_usage() {
    ssh -q -o "StrictHostKeyChecking=no" "$1" 'bash -s' <<'ENDSSH'
        if ! command -v mpstat &> /dev/null; then
            sudo apt-get install -y sysstat
        fi
        mpstat 1 1 | awk '/^Average:/ {printf "%.2f%%", 100 - $NF}'
ENDSSH
}

# Function to get memory usage
get_memory_usage() {
    ssh -q -o "StrictHostKeyChecking=no" "$1" 'bash -s' <<'ENDSSH'
        free -h | awk '/^Mem:/ {printf "Used: %s, Free: %s", $3, $4}'
ENDSSH
}

# Function to get storage usage
get_storage_usage() {
    ssh -q -o "StrictHostKeyChecking=no" "$1" 'bash -s' <<'ENDSSH'
        df -h | awk '$NF=="/" {printf "Used: %s, Available: %s", $3, $4}'
ENDSSH
}

# Parse the command line arguments
case "$1" in
    --cpu)
        get_cpu_usage "$2"
        echo "" # Add an echo command to print a new line
        ;;
    --memory)
        get_memory_usage "$2"
        echo "" # Add an echo command to print a new line
        ;;
    --storage)
        get_storage_usage "$2"
        echo "" # Add an echo command to print a new line
        ;;
    *)
        echo "Unknown option: $1"
        echo "Usage: sysget [--cpu | --memory | --storage] <IP>"
        exit 1
        ;;
esac
